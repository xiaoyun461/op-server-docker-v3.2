#!/bin/bash
echo "=============== start down data ========"
docker run --name op-data -d xiaoyun461/op-data:3.2
docker cp op-data:/usr/local/nginx/html/data.tar.gz .
docker stop op-data
docker rm -f op-data
tar -xzvf data.tar.gz
echo "=============== end down data  ========"

echo "=============== start fix data  ========"
cp -r fix/2023-03-15/* data/
echo "===============  end  fix data ========"
