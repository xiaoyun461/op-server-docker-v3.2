#!/bin/bash
mongo <<EOF
use grasscutter;
db.getCollection("accounts").drop();
db.createCollection("accounts");
db.getCollection("accounts").createIndex({
    username: NumberInt("1")
}, {
    name: "username_1",
    unique: true
});

db.getCollection("accounts").insert([ {
    _id: "10001",
    username: "123456",
    reservedPlayerId: NumberInt("0"),
    token: "4fc81b5d8802790c5865edd1849bf6865ea66171e3d2cd91e8939ad7166b59a7",
    sessionKey: "3d5eb782441e235294809934958d2cf4f37c80eeed9ab07ee5b6798feec4e8ed",
    permissions: [ ],
    locale: "zh_CN",
    banEndTime: NumberInt("0"),
    banStartTime: NumberInt("0"),
    isBanned: false
} ]);
db.getCollection("accounts").insert([ {
    _id: "10002",
    username: "xiaoxiao",
    reservedPlayerId: NumberInt("0"),
    token: "53a4ccc1a9a3064a07d65dce66be6fd60ac22e8de8b0d56cf8f954b13cb568fd",
    sessionKey: "5e903db14af20fa01c1bf379ef46492ae9c7908394c85d991af581471e1a47ff",
    permissions: [ ],
    locale: "zh_CN",
    banEndTime: NumberInt("0"),
    banStartTime: NumberInt("0"),
    isBanned: false
} ]);
db.getCollection("accounts").insert([ {
    _id: "10003",
    username: "xiaoyun",
    reservedPlayerId: NumberInt("0"),
    token: "d2b0f98151cfd18a4572f14b6d60d56ab85e2a0875c3ccbd01d69a861cbcfa56",
    sessionKey: "a127881948e7b8e5cc20ebcfa34003e9adbe14c814def596df386c8bcd848128",
    permissions: [ ],
    locale: "zh_CN",
    banEndTime: NumberInt("0"),
    banStartTime: NumberInt("0"),
    isBanned: false
} ]);

db.getCollection("counters").drop();
db.createCollection("counters");

db.getCollection("counters").insert([ {
    _id: "Account",
    count: NumberInt("10003")
} ]);
EOF
