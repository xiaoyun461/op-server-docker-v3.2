# op-server-docker-v3.2

## 免责申明

本项目来源于网络只为了学习交流之用，请不要用于任何商业目的，否则后果自负！
若由此引起的一切法律责任都与本人无关！版权都归游戏官方所有，请在文件下载后于24小时内删除！

#### 介绍

op docker server 一键构建

#### 软件架构
软件架构说明


#### 前置data下载
 ```shell
chmod a+x down_data.sh
./down_data.sh
   ```

#### 安装教程

1. 修改 `.env` 文件中 OUTER_IP_ADDR 的IP ,为服务器外网IP
2. 执行 `docker-compose up -d` 运行服务端
3. 客户端 连接 你的 `外网IP:2888` 代理,开始游戏


#### 使用说明

1. `.env` 可以修改 `mysql` 数据库ip地址,`redis` 的地址,以及`mongo`的地址,主要是 为了 方便 数据与 服务端进行隔离
2. 现在每一个服务器server 都可以单独在一个服务器上独立运行,在 .env 里可以通过设置ip
```shell
tothemoonserver
multiserver
pathfindingserver
oaserver
muipserver
gameserver
dispatch
dbgate
gateserver
nodeserver
```
其中 `tothemoonserver` `multiserver` `pathfindingserver` `oaserver` `muipserver` `gameserver`单独部署的话执行`down_data.sh` 下载data文件

`dispatch`,`dbgate`,`gateserver`,`nodeserver`这4个不需要下载data文件 都可以直接运行


3.关于如何修改服务端的配置文件txt 
数据都在项目目录 的 data文件夹下(data文件夹是在 前置data下载 后出现的)，需要修改覆盖即可
然后重启
```shell
docker-compose down
docker-compose up -d
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


