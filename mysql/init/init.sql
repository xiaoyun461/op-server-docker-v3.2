GRANT ALL PRIVILEGES ON *.* TO 'MYSQL_USER'@'127.0.0.1' IDENTIFIED BY 'MYSQL_PWD' WITH GRANT OPTION;
FLUSH   PRIVILEGES;
GRANT ALL PRIVILEGES ON *.* TO 'MYSQL_USER'@'localhost' IDENTIFIED BY 'MYSQL_PWD' WITH GRANT OPTION;
FLUSH   PRIVILEGES;
grant all privileges on *.* to 'MYSQL_USER'@'%' identified by 'MYSQL_PWD';

SET NAMES 'utf8';
SET character_set_client = utf8;
SET character_set_results = utf8;
SET character_set_connection = utf8;
SET character_set_database = utf8;

create  database  `db_hk4e_config_gio`  DEFAULT CHARACTER SET utf8;
create  database  `db_hk4e_deploy_config_gio`  DEFAULT CHARACTER SET utf8;
create  database  `db_hk4e_order_gio`  DEFAULT CHARACTER SET utf8;
create  database  `db_hk4e_user_gio`  DEFAULT CHARACTER SET utf8;
flush privileges;

use `db_hk4e_config_gio`;
source /opt/sql/db_hk4e_config_gio.sql;
flush privileges;

use `db_hk4e_deploy_config_gio`;
source /opt/sql/db_hk4e_deploy_config_gio.sql;
flush privileges;

use `db_hk4e_order_gio`;
source /opt/sql/db_hk4e_order_gio.sql;
flush privileges;

use `db_hk4e_user_gio`;
source /opt/sql/db_hk4e_user_gio.sql;
flush privileges;