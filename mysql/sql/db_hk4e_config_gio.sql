/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.2.130
 Source Server Type    : MySQL
 Source Server Version : 50741
 Source Host           : 192.168.2.130:3306
 Source Schema         : db_hk4e_config_gio

 Target Server Type    : MySQL
 Target Server Version : 50741
 File Encoding         : 65001

 Date: 18/02/2023 00:53:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_account_cancellation_config
-- ----------------------------
DROP TABLE IF EXISTS `t_account_cancellation_config`;
CREATE TABLE `t_account_cancellation_config`  (
  `uid` int(10) UNSIGNED NOT NULL COMMENT 'æ¸¸æˆuid',
  `account_uid` bigint(20) UNSIGNED NOT NULL COMMENT 'é€šè¡Œè¯aid',
  `cancellation_time` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'è´¦å·æ³¨é”€æ—¶é—´',
  `create_timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'æ›´æ–°æ—¶é—´',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'ç±³å“ˆæ¸¸é€šè¡Œè¯æ³¨é”€åå•' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_account_cancellation_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_activity_data
-- ----------------------------
DROP TABLE IF EXISTS `t_activity_data`;
CREATE TABLE `t_activity_data`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `activity_id` int(10) UNSIGNED NOT NULL,
  `schedule_id` int(10) UNSIGNED NOT NULL,
  `activity_type` int(10) UNSIGNED NOT NULL COMMENT 'æ´»åŠ¨ç±»åž‹ï¼Œé¿å…ç­–åˆ’activity_idåšæ–°çš„æ´»åŠ¨',
  `bin_data` blob NOT NULL COMMENT 'ä½¿ç”¨protobufåºåˆ—åŒ–åŽçš„äºŒè¿›åˆ¶å­—æ®µ',
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `activity_schedule_id`(`activity_id`, `schedule_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'å…¨æœæ´»åŠ¨å­˜æ¡£æ•°æ®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_activity_data
-- ----------------------------

-- ----------------------------
-- Table structure for t_activity_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_activity_schedule_config`;
CREATE TABLE `t_activity_schedule_config`  (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'æŽ’æœŸID',
  `begin_time` datetime NOT NULL COMMENT 'å¼€å§‹æ—¶é—´',
  `end_time` datetime NOT NULL COMMENT 'ç»“æŸæ—¶é—´',
  PRIMARY KEY (`schedule_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'æ´»åŠ¨æŽ’æœŸè¡¨' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_activity_schedule_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_announce_config
-- ----------------------------
DROP TABLE IF EXISTS `t_announce_config`;
CREATE TABLE `t_announce_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `begin_time` datetime NOT NULL COMMENT 'å¼€å§‹æ—¶é—´',
  `end_time` datetime NOT NULL COMMENT 'ç»“æŸæ—¶é—´',
  `center_system_text` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'ä¸­å¤®ç³»ç»Ÿæç¤ºæ–‡æœ¬',
  `count_down_text` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'å€’è®¡æ—¶æç¤ºæ–‡æœ¬',
  `dungeon_confirm_text` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'åœ°ä¸‹åŸŽç¡®è®¤æ¡†æ–‡æœ¬',
  `center_system_frequency` int(11) NOT NULL COMMENT 'è·‘é©¬ç¯é¢‘çŽ‡',
  `count_down_frequency` int(11) NOT NULL COMMENT 'å€’è®¡æ—¶é¢‘çŽ‡',
  `channel_config_str` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'æ¸ é“é…ç½®',
  `is_center_system_last_5_every_minutes` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'è·‘é©¬ç¯æœ€åŽ5åˆ†é’Ÿæ¯åˆ†é’Ÿé€šçŸ¥',
  `channel_id_list` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'æ¸ é“IDåˆ—è¡¨',
  `platform_type_list` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'å®¢æˆ·ç«¯å¹³å°ç±»åž‹',
  `enable` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'æ˜¯å¦æœ‰æ•ˆ',
  `server_version` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'é¢„å‘ŠåŠŸèƒ½é…ç½®è¡¨' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_announce_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_anti_offline_whitelist
-- ----------------------------
DROP TABLE IF EXISTS `t_anti_offline_whitelist`;
CREATE TABLE `t_anti_offline_whitelist`  (
  `uid` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'åè„±æœºæŒ‚å¼ºå¯¹æŠ—ç™½åå•' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_anti_offline_whitelist
-- ----------------------------

-- ----------------------------
-- Table structure for t_battle_pass_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_battle_pass_schedule_config`;
CREATE TABLE `t_battle_pass_schedule_config`  (
  `schedule_id` int(11) NOT NULL COMMENT 'æŽ’æœŸID, ä¸ŽExcelä¸­é…ç½®ä¸€è‡´',
  `begin_date` date NOT NULL COMMENT 'å¼€å§‹æ—¥æœŸ',
  `end_date` date NOT NULL COMMENT 'ç»“æŸæ—¥æœŸ',
  PRIMARY KEY (`schedule_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'æˆ˜ä»¤(BattlePass)æŽ’æœŸé…ç½®è¡¨' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_battle_pass_schedule_config
-- ----------------------------
INSERT INTO `t_battle_pass_schedule_config` VALUES (1100, '2023-01-01', '2023-02-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (1200, '2023-02-01', '2023-03-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (1300, '2023-03-01', '2023-04-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (1400, '2023-04-01', '2023-05-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (1500, '2023-05-01', '2023-06-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (1600, '2023-06-01', '2023-07-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (2000, '2023-07-01', '2023-08-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (2100, '2023-08-01', '2023-09-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (2200, '2023-09-01', '2023-10-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (2300, '2023-10-01', '2023-11-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (2400, '2023-11-01', '2023-12-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (2500, '2023-12-01', '2024-01-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (2600, '2024-01-01', '2024-02-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (2700, '2024-02-01', '2024-03-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (2800, '2024-03-01', '2024-04-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (3000, '2024-04-01', '2024-05-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (3100, '2024-05-01', '2024-06-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (3200, '2024-06-01', '2024-07-01');
INSERT INTO `t_battle_pass_schedule_config` VALUES (9998, '2022-12-01', '2023-01-01');

-- ----------------------------
-- Table structure for t_chat_block_config
-- ----------------------------
DROP TABLE IF EXISTS `t_chat_block_config`;
CREATE TABLE `t_chat_block_config`  (
  `uid` int(10) UNSIGNED NOT NULL,
  `end_time` datetime NOT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'çŽ©å®¶ç¦è¨€é…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_chat_block_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_client_watchdog_uid_list_config
-- ----------------------------
DROP TABLE IF EXISTS `t_client_watchdog_uid_list_config`;
CREATE TABLE `t_client_watchdog_uid_list_config`  (
  `uid` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'watchdogå¼€å¯åå•' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_client_watchdog_uid_list_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_cmd_frequency_config
-- ----------------------------
DROP TABLE IF EXISTS `t_cmd_frequency_config`;
CREATE TABLE `t_cmd_frequency_config`  (
  `cmd_id` int(10) UNSIGNED NOT NULL,
  `frequency_limit` float NOT NULL COMMENT 'å•ä½æ—¶é—´å†…æœ€å¤§æ”¶åŒ…é‡',
  `discard_packet_freq_limit` float NOT NULL COMMENT 'è¶…è¿‡æ­¤é¢‘çŽ‡æ—¶ä¸¢å¼ƒæœ¬æ¬¡åè®®åŒ…',
  `disconnect_freq_limit` float NOT NULL COMMENT 'è¶…è¿‡æ­¤é¢‘çŽ‡æ—¶è¸¢çŽ©å®¶ä¸‹çº¿'
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'åè®®é¢‘çŽ‡é™åˆ¶é…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_cmd_frequency_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_cmd_str_frequency_config
-- ----------------------------
DROP TABLE IF EXISTS `t_cmd_str_frequency_config`;
CREATE TABLE `t_cmd_str_frequency_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cmd_str` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'é€šä¿¡åŒ…å',
  `frequency_limit` float NOT NULL COMMENT 'å•ä½æ—¶é—´å†…æœ€å¤§æ”¶åŒ…é‡',
  `discard_packet_freq_limit` float NOT NULL COMMENT 'è¶…è¿‡æ­¤é¢‘çŽ‡æ—¶ä¸¢å¼ƒæœ¬æ¬¡åè®®åŒ…',
  `disconnect_freq_limit` float NOT NULL COMMENT 'è¶…è¿‡æ­¤é¢‘çŽ‡æ—¶è¸¢çŽ©å®¶ä¸‹çº¿',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'åè®®é¢‘çŽ‡é™åˆ¶é…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_cmd_str_frequency_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_feature_block_config
-- ----------------------------
DROP TABLE IF EXISTS `t_feature_block_config`;
CREATE TABLE `t_feature_block_config`  (
  `uid` int(10) UNSIGNED NOT NULL,
  `type` int(10) UNSIGNED NOT NULL,
  `end_time` datetime NOT NULL,
  `begin_time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`uid`, `type`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'çŽ©å®¶çŽ©æ³•å°ç¦é…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_feature_block_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_feature_switch_off_config
-- ----------------------------
DROP TABLE IF EXISTS `t_feature_switch_off_config`;
CREATE TABLE `t_feature_switch_off_config`  (
  `id` int(10) UNSIGNED NOT NULL,
  `type` int(10) UNSIGNED NOT NULL,
  `msg` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'å…³é—­ç³»ç»Ÿå¼€å…³è¡¨' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_feature_switch_off_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_gacha_newbie_url_config
-- ----------------------------
DROP TABLE IF EXISTS `t_gacha_newbie_url_config`;
CREATE TABLE `t_gacha_newbie_url_config`  (
  `priority` int(10) UNSIGNED NOT NULL COMMENT 'ä¼˜å…ˆçº§',
  `gacha_prob_url` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ‰­è›‹æ¦‚çŽ‡å±•ç¤ºurl',
  `gacha_record_url` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ‰­è›‹è®°å½•url',
  PRIMARY KEY (`priority`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'æ–°æ‰‹æ‰­è›‹urlé…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_gacha_newbie_url_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_gacha_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_gacha_schedule_config`;
CREATE TABLE `t_gacha_schedule_config`  (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'æ´»åŠ¨ID',
  `gacha_type` int(11) NOT NULL DEFAULT 0 COMMENT 'æ‰­è›‹ç±»åž‹',
  `begin_time` datetime NOT NULL COMMENT 'å¼€å§‹æ—¶é—´',
  `end_time` datetime NOT NULL COMMENT 'ç»“æŸæ—¶é—´',
  `cost_item_id` int(10) UNSIGNED NOT NULL COMMENT 'æ¶ˆè€—ææ–™ID',
  `cost_item_num` int(10) UNSIGNED NOT NULL COMMENT 'æ¶ˆè€—ææ–™æ•°é‡',
  `gacha_pool_id` int(10) UNSIGNED NOT NULL COMMENT 'Gachaæ ¹ID',
  `gacha_prob_rule_id` int(10) UNSIGNED NOT NULL COMMENT 'Gachaæ¦‚çŽ‡é…ç½®ID',
  `gacha_up_config` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'UPé…ç½®',
  `gacha_rule_config` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'ä¿åº•è§„åˆ™é…ç½®',
  `gacha_prefab_path` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ‰­è›‹Prefabè·¯å¾„',
  `gacha_preview_prefab_path` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ‰­è›‹é¢„è§ˆPrefabè·¯å¾„',
  `gacha_prob_url` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ‰­è›‹æ¦‚çŽ‡å±•ç¤ºurl',
  `gacha_record_url` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ‰­è›‹è®°å½•url',
  `gacha_prob_url_oversea` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æµ·å¤–æ‰­è›‹æ¦‚çŽ‡å±•ç¤ºurl',
  `gacha_record_url_oversea` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æµ·å¤–æ‰­è›‹è®°å½•url',
  `gacha_sort_id` int(10) UNSIGNED NOT NULL COMMENT 'æ‰­è›‹æŽ’åºæƒé‡',
  `enabled` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0ä¸ç”Ÿæ•ˆï¼Œ1ç”Ÿæ•ˆ',
  `title_textmap` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'Gachaæ˜¾ç¤ºå¤šè¯­è¨€æ–‡æœ¬',
  `display_up4_item_list` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ˜¾ç¤ºup4æ˜Ÿç‰©å“',
  PRIMARY KEY (`schedule_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 924 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'æ‰­è›‹æ´»åŠ¨é…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_gacha_schedule_config
-- ----------------------------
INSERT INTO `t_gacha_schedule_config` VALUES (893, 200, '2022-11-02 13:37:46', '2023-11-30 13:37:55', 224, 1, 201, 1, '{\"gacha_up_list\":[{\"item_parent_type\":1,\"prob\":500,\"item_list\":[1002,1003,1016,1022,1026,1029,1030,1033,1035,1037,1038,1041,1042,1046,1047,1049,1051,1052,1054,1057,1058,1063,1066]},{\"item_parent_type\":2,\"prob\":500,\"item_list\":[1020,1032,1034]}]}', '{}', 'GachaShowPanel_A022', 'UI_Tab_GachaShowPanel_A022', 'http://localhost/', 'http://localhost/', 'http://localhost/', 'http://localhost/', 1000, 1, 'UI_GACHA_SHOW_PANEL_A022_TITLE', '');
INSERT INTO `t_gacha_schedule_config` VALUES (903, 301, '2022-11-02 13:37:46', '2023-11-30 13:37:55', 223, 1, 201, 1, '{\"gacha_up_list\":[{\"item_parent_type\":1,\"prob\":500,\"item_list\":[1073]},{\"item_parent_type\":2,\"prob\":500,\"item_list\":[1020,1032,1034]}]}', '{}', 'GachaShowPanel_A103', 'UI_Tab_GachaShowPanel_A103', 'http://localhost/', 'http://localhost/', 'http://localhost/', 'http://localhost/', 3001, 1, 'UI_GACHA_SHOW_PANEL_A0103_TITLE', '');
INSERT INTO `t_gacha_schedule_config` VALUES (913, 302, '2022-11-02 13:37:46', '2023-11-30 13:37:55', 223, 1, 201, 1, '{\"gacha_up_list\":[{\"item_parent_type\":1,\"prob\":500,\"item_list\":[14511,15509]},{\"item_parent_type\":2,\"prob\":500,\"item_list\":[11410,12405,13407,14402,15412]}]}', '{}', 'GachaShowPanel_A105', 'UI_Tab_GachaShowPanel_A105', 'http://localhost/', 'http://localhost/', 'http://localhost/', 'http://localhost/', 3000, 1, 'UI_GACHA_SHOW_PANEL_A021_TITLE', '');
INSERT INTO `t_gacha_schedule_config` VALUES (923, 400, '2022-11-02 13:37:46', '2023-11-30 13:37:55', 223, 1, 201, 1, '{\"gacha_up_list\":[{\"item_parent_type\":1,\"prob\":500,\"item_list\":[1049]},{\"item_parent_type\":2,\"prob\":500,\"item_list\":[1020,1032,1034]}]}', '{}', 'GachaShowPanel_A104', 'UI_Tab_GachaShowPanel_A104', 'http://localhost/', 'http://localhost/', 'http://localhost/', 'http://localhost/', 3000, 1, 'UI_GACHA_SHOW_PANEL_A049_TITLE', '');

-- ----------------------------
-- Table structure for t_gameplay_recommendation_config
-- ----------------------------
DROP TABLE IF EXISTS `t_gameplay_recommendation_config`;
CREATE TABLE `t_gameplay_recommendation_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'æ¡ç›®ID',
  `begin_time` datetime NOT NULL COMMENT 'ç”Ÿæ•ˆæ—¶é—´',
  `json_str` mediumtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'å®šä¹‰ä¸ºproto::GameplayRecommendationConfig',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0ä¸ç”Ÿæ•ˆï¼Œ1ç”Ÿæ•ˆ',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `begin_time`(`begin_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'å…»æˆæŽ¨èæ•°æ®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_gameplay_recommendation_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_h5_activity_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_h5_activity_schedule_config`;
CREATE TABLE `t_h5_activity_schedule_config`  (
  `schedule_id` int(11) NOT NULL COMMENT 'æŽ’æœŸID',
  `activity_id` int(11) NOT NULL COMMENT 'æ´»åŠ¨ID',
  `begin_time` datetime NOT NULL COMMENT 'å¼€å§‹æ—¶é—´',
  `end_time` datetime NOT NULL COMMENT 'ç»“æŸæ—¶é—´',
  `content_close_time` datetime NOT NULL COMMENT 'çŽ©æ³•ç»“æŸæ—¶é—´',
  `prefab_path` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ´»åŠ¨åº•å›¾æ–‡ä»¶',
  `url_cn` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ´»åŠ¨é“¾æŽ¥ï¼ˆå›½å†…ï¼‰',
  `url_os` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ´»åŠ¨é“¾æŽ¥ï¼ˆæµ·å¤–ï¼‰',
  `is_entrance_open` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'å…¥å£å¼€å…³ï¼š0å…³é—­ï¼Œ1å¼€æ”¾',
  PRIMARY KEY (`schedule_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'H5æ´»åŠ¨æŽ’æœŸé…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_h5_activity_schedule_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_inject_fix_config
-- ----------------------------
DROP TABLE IF EXISTS `t_inject_fix_config`;
CREATE TABLE `t_inject_fix_config`  (
  `config_id` int(10) UNSIGNED NOT NULL,
  `inject_fix` blob NOT NULL COMMENT 'inject_fix',
  `uid_list` varchar(4096) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'ç™½åå•å¤šä¸ª,éš”å¼€ï¼Œç©ºè¡¨ç¤ºä¸å¼€å¯ç™½åå•',
  `platform_type_list` varchar(4096) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT '(å¦‚å®‰å“Â ios)Â å¤šä¸ª,éš”å¼€ï¼Œç©ºè¡¨ç¤ºå¯¹å¹³å°ä¸åšè¦æ±‚',
  `percent` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'ç°åº¦ç™¾åˆ†æ¯”',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'åˆ›å»ºæ—¶é—´',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'inject_fix é…ç½®è¡¨' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_inject_fix_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_live_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_live_schedule_config`;
CREATE TABLE `t_live_schedule_config`  (
  `live_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ç›´æ’­ID',
  `begin_time` datetime NOT NULL COMMENT 'å¼€å§‹æ—¶é—´',
  `end_time` datetime NOT NULL COMMENT 'ç»“æŸæ—¶é—´',
  `live_url` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'ç›´æ’­åœ°å€',
  `spare_live_url` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'å¤‡ç”¨ç›´æ’­åœ°å€',
  PRIMARY KEY (`live_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'ç›´æ’­æŽ’æœŸè¡¨' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_live_schedule_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_login_black_ip_config
-- ----------------------------
DROP TABLE IF EXISTS `t_login_black_ip_config`;
CREATE TABLE `t_login_black_ip_config`  (
  `ip` int(10) UNSIGNED NOT NULL,
  `ip_str` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'å¯¹åº”çš„å­—ç¬¦ä¸²',
  PRIMARY KEY (`ip`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'æ³¨å†Œ&ç™»å½•ipé»‘åå•' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_login_black_ip_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_login_black_uid_config
-- ----------------------------
DROP TABLE IF EXISTS `t_login_black_uid_config`;
CREATE TABLE `t_login_black_uid_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `begin_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `msg` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uid`(`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'ç™»å…¥é»‘åå•é…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_login_black_uid_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_login_reward_config
-- ----------------------------
DROP TABLE IF EXISTS `t_login_reward_config`;
CREATE TABLE `t_login_reward_config`  (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_type` tinyint(4) NOT NULL DEFAULT 0,
  `reward_rules` varchar(1024) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email_valid_days` int(11) NOT NULL,
  `email_title` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email_sender` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email_content` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `item_list` varchar(1024) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'å¥–åŠ±åˆ—è¡¨ï¼Œproto3çš„jsonæ ¼å¼',
  `effective_account_type_list` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `begin_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0ä¸ç”Ÿæ•ˆï¼Œ1ç”Ÿæ•ˆ',
  `tag` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ ‡ç­¾',
  `importance` int(11) NOT NULL DEFAULT 0,
  `is_collectible` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0ä¸å¯æ”¶è—ï¼Œ1å¯æ”¶è—',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'æ¯æ—¥ç™»å…¥å¥–åŠ±é…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_login_reward_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_luashell_config
-- ----------------------------
DROP TABLE IF EXISTS `t_luashell_config`;
CREATE TABLE `t_luashell_config`  (
  `luashell_config_id` int(10) UNSIGNED NOT NULL,
  `lua_shell` mediumblob NOT NULL COMMENT 'luaè„šæœ¬',
  `uid_list` varchar(4096) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'ç™½åå•å¤šä¸ª,éš”å¼€ï¼Œç©ºè¡¨ç¤ºä¸å¼€å¯ç™½åå•',
  `platform_type_list` varchar(4096) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT '(å¦‚å®‰å“Â ios)Â å¤šä¸ª,éš”å¼€ï¼Œç©ºè¡¨ç¤ºå¯¹å¹³å°ä¸åšè¦æ±‚',
  `percent` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'ç°åº¦ç™¾åˆ†æ¯”',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'åˆ›å»ºæ—¶é—´',
  `protocol_type` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'åè®®ç±»åž‹',
  `use_type` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'ç”¨äºŽæ ‡è¯†luashellçš„ç”¨é€”ï¼š1.æ™®é€šluashellï¼›2.å®‰å…¨åº“lua',
  `is_check_client_report` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'æ˜¯å¦æ£€æŸ¥å®¢æˆ·ç«¯å›žå¤ä¸ŠæŠ¥',
  `is_kick` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'æ£€æŸ¥å®¢æˆ·ç«¯å›žå¤å¤±è´¥åŽæ˜¯å¦è¸¢ä¸‹çº¿',
  `check_json_key` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ£€æŸ¥å®¢æˆ·ç«¯å›žå¤çš„keyçš„å­—ç¬¦ä¸²',
  `channel` int(11) NOT NULL DEFAULT 0 COMMENT 'ä¸‹å‘é€šé“',
  PRIMARY KEY (`luashell_config_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'luashell é…ç½®è¡¨' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_luashell_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_mail_block_tag_config
-- ----------------------------
DROP TABLE IF EXISTS `t_mail_block_tag_config`;
CREATE TABLE `t_mail_block_tag_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tag` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'é‚®ä»¶å±è”½æ ‡ç­¾é…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_mail_block_tag_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_mtp_blacklist_config
-- ----------------------------
DROP TABLE IF EXISTS `t_mtp_blacklist_config`;
CREATE TABLE `t_mtp_blacklist_config`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'é»˜è®¤ä¸»é”®',
  `type` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'å¯¹æŠ—ç±»åž‹',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'MTPè¦è¸¢ä¸‹çº¿çš„é»‘åå•ID' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_mtp_blacklist_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_mtp_whitelist_config
-- ----------------------------
DROP TABLE IF EXISTS `t_mtp_whitelist_config`;
CREATE TABLE `t_mtp_whitelist_config`  (
  `id` int(11) NOT NULL COMMENT 'mtpä¸ŠæŠ¥id',
  `reason` varchar(512) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'mtpä¸ŠæŠ¥idå¯¹åº”çš„reason',
  `match_type` int(11) NOT NULL COMMENT 'åŒ¹é…ç±»åž‹ï¼š1ã€2ã€3åˆ†åˆ«è¡¨ç¤ºåŒ…å«ã€å¼€å¤´ã€å•ä¸€'
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'MTPç™½åå•' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_mtp_whitelist_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_op_activity_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_op_activity_schedule_config`;
CREATE TABLE `t_op_activity_schedule_config`  (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'æŽ’æœŸID',
  `config_id` int(11) NOT NULL DEFAULT 0 COMMENT 'æ´»åŠ¨é…ç½®ID',
  `begin_time` datetime NOT NULL COMMENT 'å¼€å§‹æ—¶é—´',
  `end_time` datetime NOT NULL COMMENT 'ç»“æŸæ—¶é—´',
  PRIMARY KEY (`schedule_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'è¿è¥æ´»åŠ¨é…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_op_activity_schedule_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_questionnaire_mail_config
-- ----------------------------
DROP TABLE IF EXISTS `t_questionnaire_mail_config`;
CREATE TABLE `t_questionnaire_mail_config`  (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_valid_days` int(11) NOT NULL,
  `email_title` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email_sender` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email_content` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `item_list` varchar(1024) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'å¥–åŠ±åˆ—è¡¨ï¼Œproto3çš„jsonæ ¼å¼',
  `begin_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0ä¸ç”Ÿæ•ˆï¼Œ1ç”Ÿæ•ˆ',
  `tag` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ ‡ç­¾',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'é‚®ä»¶é…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_questionnaire_mail_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_rebate_config
-- ----------------------------
DROP TABLE IF EXISTS `t_rebate_config`;
CREATE TABLE `t_rebate_config`  (
  `account_type` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'è´¦å·ç±»åž‹',
  `account_uid` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'ç»‘å®šçš„è´¦å·UID',
  `item_list` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'å……å€¼è¿”åˆ©é“å…·åˆ—è¡¨ï¼Œå…ˆé€—å·å†å†’å·åˆ†éš”',
  PRIMARY KEY (`account_type`, `account_uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'å……å€¼è¿”åˆ©åå•' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_rebate_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_red_point_config
-- ----------------------------
DROP TABLE IF EXISTS `t_red_point_config`;
CREATE TABLE `t_red_point_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'æ¡ç›®ID',
  `content_id` int(10) UNSIGNED NOT NULL COMMENT 'æ´»åŠ¨IDæˆ–è°ƒæŸ¥é—®å·IDç­‰äºŒçº§ID',
  `trigger_time` datetime NOT NULL COMMENT 'è§¦å‘æ—¶é—´',
  `expire_time` datetime NOT NULL COMMENT 'å¤±æ•ˆæ—¶é—´',
  `red_point_type` int(10) UNSIGNED NOT NULL COMMENT 'çº¢ç‚¹ç±»åž‹ï¼ˆçº¢ç‚¹ID/çº¢ç‚¹ä½keyï¼‰',
  `is_daily_refresh` int(10) UNSIGNED NOT NULL COMMENT 'æ˜¯å¦è¿›è¡Œæ¯æ—¥åˆ·æ–°',
  `daily_refresh_second` int(10) UNSIGNED NOT NULL COMMENT 'æ¯å¤©0ç‚¹å¼€å§‹çš„ç¬¬å‡ ç§’è¿›è¡Œæ¯æ—¥åˆ·æ–°',
  `player_level` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'æœ€å°çŽ©å®¶ç­‰çº§',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'å…¨æœçº¢ç‚¹é…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_red_point_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_register_black_ip_config
-- ----------------------------
DROP TABLE IF EXISTS `t_register_black_ip_config`;
CREATE TABLE `t_register_black_ip_config`  (
  `ip` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ip_desc` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'IPåœ°å€å¤‡æ³¨ä¿¡æ¯',
  PRIMARY KEY (`ip`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'æ³¨å†Œipé»‘åå•' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_register_black_ip_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_security_library_config
-- ----------------------------
DROP TABLE IF EXISTS `t_security_library_config`;
CREATE TABLE `t_security_library_config`  (
  `platform_type_str` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'å¹³å°ç±»åž‹ï¼Œå®šä¹‰åœ¨define.protoçš„PlatformType',
  `version_str` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'ç‰ˆæœ¬å·',
  `md5_list` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'md5æ ¡éªŒå€¼ï¼Œé€—å·åˆ†éš”',
  `is_forbid_login` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'MD5ä¸ä¸€è‡´æ—¶æ˜¯å¦ç¦æ­¢ç™»å½•',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0ä¸ç”Ÿæ•ˆï¼Œ1ç”Ÿæ•ˆ',
  PRIMARY KEY (`platform_type_str`, `version_str`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'å®‰å…¨åº“é…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_security_library_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_sign_in_schedule_config
-- ----------------------------
DROP TABLE IF EXISTS `t_sign_in_schedule_config`;
CREATE TABLE `t_sign_in_schedule_config`  (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'æŽ’æœŸID',
  `config_id` int(11) NOT NULL DEFAULT 0 COMMENT 'ç­¾åˆ°é…ç½®ID',
  `begin_time` datetime NOT NULL COMMENT 'å¼€å§‹æ—¶é—´',
  `end_time` datetime NOT NULL COMMENT 'ç»“æŸæ—¶é—´',
  PRIMARY KEY (`schedule_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'ç­¾åˆ°æ´»åŠ¨é…ç½®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sign_in_schedule_config
-- ----------------------------
INSERT INTO `t_sign_in_schedule_config` VALUES (1, 1001, '2022-12-22 00:48:47', '2023-01-06 00:48:50');
INSERT INTO `t_sign_in_schedule_config` VALUES (2, 1002, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (3, 10001, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (4, 10002, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (5, 10003, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (6, 10004, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (7, 10005, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (8, 10006, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (9, 10007, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (10, 10008, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (11, 10009, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (12, 10010, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (13, 10011, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (14, 10012, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (15, 10013, '2022-12-22 00:49:02', '2023-01-06 00:49:06');
INSERT INTO `t_sign_in_schedule_config` VALUES (16, 10014, '2022-12-22 00:49:02', '2023-01-06 00:49:06');

-- ----------------------------
-- Table structure for t_stop_server_login_white_ip_config
-- ----------------------------
DROP TABLE IF EXISTS `t_stop_server_login_white_ip_config`;
CREATE TABLE `t_stop_server_login_white_ip_config`  (
  `ip` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `desc` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`ip`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'åœæœæ—¶äºŒçº§dispatchç™»å½•ç™½åå•' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_stop_server_login_white_ip_config
-- ----------------------------

-- ----------------------------
-- Table structure for t_textmap_config
-- ----------------------------
DROP TABLE IF EXISTS `t_textmap_config`;
CREATE TABLE `t_textmap_config`  (
  `text_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'textmapçš„key',
  `delete_time` datetime NOT NULL COMMENT 'å¤±æ•ˆæ—¶é—´ï¼Œæ—¶é—´ä¸€åˆ°å°±ä¼šåˆ é™¤è¿™æ¡è®°å½•',
  `en` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'è‹±æ–‡',
  `sc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'ç®€ä½“ä¸­æ–‡',
  `tc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'ç¹ä½“ä¸­æ–‡',
  `fr` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'æ³•è¯­',
  `de` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'å¾·è¯­',
  `es` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'è¥¿ç­ç‰™è¯­',
  `pt` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'è‘¡è„ç‰™è¯­',
  `ru` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'ä¿„è¯­',
  `jp` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'æ—¥è¯­',
  `kr` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'éŸ©è¯­',
  `th` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'æ³°æ–‡',
  `vn` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'è¶Šå—è¯­',
  `id` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'å°å°¼è¯­',
  `tr` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'åœŸè€³å…¶è¯­',
  `it` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'æ„å¤§åˆ©è¯­',
  PRIMARY KEY (`text_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'æœåŠ¡ç«¯çš„textmapä¸€èˆ¬ç”¨äºŽé‚®ä»¶ï¼Œéœ€è¦æŽ§åˆ¶æ¡ç›®ï¼Œå› ä¸ºå…¨éƒ¨åŠ è½½åˆ°å†…å­˜ä¸­' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_textmap_config
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
