/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.2.130
 Source Server Type    : MySQL
 Source Server Version : 50741
 Source Host           : 192.168.2.130:3306
 Source Schema         : db_hk4e_order_gio

 Target Server Type    : MySQL
 Target Server Version : 50741
 File Encoding         : 65001

 Date: 18/02/2023 00:54:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_order_data
-- ----------------------------
DROP TABLE IF EXISTS `t_order_data`;
CREATE TABLE `t_order_data`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'è‡ªå¢žID',
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'çŽ©å®¶UID',
  `product_id` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'é€ä¼ çš„å•†å“id',
  `product_name` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'å•†å“åç§°',
  `product_num` int(11) NOT NULL DEFAULT 0 COMMENT 'å•†å“æ•°é‡',
  `coin_num` int(11) NOT NULL DEFAULT 0 COMMENT 'è‡ªå®šä¹‰å……å€¼æ°´æ™¶',
  `total_fee` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0' COMMENT 'è®¢å•é‡‘é¢',
  `currency` varchar(16) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'å¸ç§',
  `price_tier` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'ä»·æ ¼æ¡£ä½',
  `trade_no` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æµæ°´å·å…¨å±€å”¯ä¸€',
  `trade_time` int(11) NOT NULL DEFAULT 0 COMMENT 'äº¤æ˜“æ—¶é—´-SDKä¼ æ¥çš„æ—¶é—´æˆ³',
  `channel_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'æ¸ é“id',
  `channel_order_no` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'å¤–éƒ¨è®¢å•å·ï¼Œæ¸ é“+è®¢å•å·å”¯ä¸€ç¡®å®šä¸€ä¸ªè®¢å•',
  `pay_plat` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ”¯ä»˜æ¸ é“',
  `extend` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'å…¶å®ƒä¿¡æ¯',
  `env` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'sandbox - æ²™ç®±çŽ¯å¢ƒ, prod - ç”Ÿäº§çŽ¯å¢ƒ, prod-os - æµ·å¤–ç”Ÿäº§çŽ¯å¢ƒï¼ˆå…¶ä»–å€¼è¯·å¿½ç•¥ï¼‰',
  `is_sandbox` int(11) NOT NULL DEFAULT 0 COMMENT '1-æ²™ç®±æ”¯ä»˜ï¼ŒéžçœŸé’±ï¼› 0-çœŸå®žæ”¯ä»˜',
  `region` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'åŒºæœå',
  `bonus` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'é¢å¤–èµ é€ä»£å¸å',
  `bonus_num` int(11) NOT NULL DEFAULT 0 COMMENT 'é¢å¤–èµ é€ä»£å¸æ•°é‡',
  `vip_point_num` int(11) NOT NULL DEFAULT 0 COMMENT 'é¢å¤–èµ é€çš„vipç‚¹æ•°',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'oaserverç”Ÿæˆè®¢å•çš„æ—¶é—´',
  `finish_time` int(11) NULL DEFAULT 0 COMMENT 'gameserverç»“ç®—è®¢å•çš„æ—¶é—´',
  `pay_type` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ”¯ä»˜æ–¹å¼ï¼Œä»…ç”¨äºŽæ•°æ®å¹³å°åˆ†æž',
  `pay_vendor` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'æ”¯ä»˜å‘å¡æœºæž„ï¼Œä»…ç”¨äºŽæ•°æ®å¹³å°åˆ†æž',
  `client_type` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'å……å€¼æ—¶ä½¿ç”¨çš„è®¾å¤‡ç±»ï¼Œä»…ç”¨äºŽæ•°æ®å¹³å°åˆ†æž',
  `device` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'å……å€¼æ—¶ä½¿ç”¨çš„è®¾å¤‡ï¼Œä»…ç”¨äºŽæ•°æ®å¹³å°åˆ†æž',
  `client_ip` varchar(46) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT 'å®¢æˆ·ç«¯IPåœ°å€',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `trade_no`(`trade_no`) USING BTREE,
  INDEX `uid_create_finish_time`(`uid`, `create_time`, `finish_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'è®¢å•æ•°æ®' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_order_data
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
