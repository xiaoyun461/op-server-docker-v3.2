#!/bin/bash
echo "===============build data to docker ~========"
docker build -f data.Dockerfile -t xiaoyun461/op-data:3.2 .
docker build -f base.Dockerfile -t xiaoyun461/op-base-images:3.2 .
docker push xiaoyun461/op-data:3.2
docker push xiaoyun461/op-base-images:3.2
docker push xiaoyun461/op-tothemoonserver:3.2
docker push xiaoyun461/op-multiserver:3.2
docker push xiaoyun461/op-pathfindingserver:3.2
docker push xiaoyun461/op-oaserver:3.2
docker push xiaoyun461/op-muipserver:3.2
docker push xiaoyun461/op-gameserver:3.2
docker push xiaoyun461/op-dispatch:3.2
docker push xiaoyun461/op-dbgate:3.2
docker push xiaoyun461/op-gateserver:3.2
docker push xiaoyun461/op-nodeserver:3.2