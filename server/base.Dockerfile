############## 游戏服务器 #####################
FROM debian:buster-slim as base-images
MAINTAINER biebbwa@163.com

COPY base/wait-for-it.sh /usr/local/bin
COPY base/gameserver/docker-entrypoint.sh /usr/local/bin
ADD lib.tar.gz /srv

RUN sed -i s@/deb.debian.org/@/mirrors.tuna.tsinghua.edu.cn/@g /etc/apt/sources.list \
    && ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
    && set -eux \
    && apt-get update \
    && apt-get install -y --no-install-recommends python2.7-dev python-pip \
    && rm -rf /var/lib/apt/lists/* \
    && ln -s /srv/lib/libasan.so.5 /usr/lib/x86_64-linux-gnu/libasan.so.5 \
    && ln -s /srv/lib/libgcc_s.so.1 /usr/lib/x86_64-linux-gnu/libgcc_s.so.1 \
    && rm -rf /usr/lib/x86_64-linux-gnu/libstdc++.so.6 \
    && ln -s /srv/lib/libstdc++.so.6 /usr/lib/x86_64-linux-gnu/libstdc++.so.6 \
    && ln -s /srv/lib/libstdc++.so.6 /usr/lib/x86_64-linux-gnu/libstdc++.so.6.25 \
    && ldconfig -v

