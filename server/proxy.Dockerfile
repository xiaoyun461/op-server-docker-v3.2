############## 代理转发服务器-proxy #####################
FROM eclipse-temurin:18-jre as proxy-server
MAINTAINER biebbwa@163.com
ENV TZ Asia/Shanghai
COPY base/wait-for-it.sh /usr/local/bin
COPY base/proxyserver/config/config.json /base-config/config.json
COPY base/proxyserver/docker-entrypoint.sh /usr/local/bin
ADD base/proxyserver/proxy-server.tar.gz /

ENV TZ=Asia/Shanghai \
    DEBIAN_FRONTEND=noninteractive
RUN  sed -i s@/archive.ubuntu.com/@/mirrors.tuna.tsinghua.edu.cn/@g /etc/apt/sources.list \
     && apt update \
     && apt install -y tzdata \
     && ln -fs /usr/share/zoneinfo/${TZ} /etc/localtime \
     && echo ${TZ} > /etc/timezone \
     && dpkg-reconfigure --frontend noninteractive tzdata \
     && rm -rf /var/lib/apt/lists/* \
     && chmod +x /usr/local/bin/wait-for-it.sh \
     && chmod +x /usr/local/bin/docker-entrypoint.sh

WORKDIR /
ENTRYPOINT docker-entrypoint.sh