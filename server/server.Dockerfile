FROM xiaoyun461/op-base-images:3.2 as base-server
ARG SERVER_NAME
ENV SERVER_NAME ${SERVER_NAME}
ADD ${SERVER_NAME}.tar.gz /srv
COPY base/gameserver/config/${SERVER_NAME}.xml /srv/bak/${SERVER_NAME}.xml
RUN  chmod +x /usr/local/bin/wait-for-it.sh && \
     chmod +x /usr/local/bin/docker-entrypoint.sh

WORKDIR /srv/${SERVER_NAME}
ENTRYPOINT wait-for-it.sh  ${SERVER_WAIT} -s -t 0 -- docker-entrypoint.sh
