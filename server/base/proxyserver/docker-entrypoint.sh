#!/bin/bash
echo "start up ~"

sleep 1

cd /base-config
find . -name "config.json"  -exec sed -i "s/OUTER_IP_ADDR/$OUTER_IP_ADDR/g" {} +
find . -name "config.json"  -exec sed -i "s/MONGODB_HOST/$MONGODB_HOST/g" {} +
find . -name "config.json"  -exec sed -i "s/MONGODB_PORT/$MONGODB_PORT/g" {} +
cp /base-config/config.json /
echo "==============================="

cd /

wait-for-it.sh ${PROXYSERVER_WAIT} -s -t 0 -- java -jar ${JAVA_OPTS} sdkserver.jar
