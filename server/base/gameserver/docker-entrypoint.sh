#!/bin/bash
echo "=========================================="

cd /srv/bak
find . -name "*.xml"  -exec sed -i "s/MYSQL_HOST/$MYSQL_HOST/g" {} +
find . -name "*.xml"  -exec sed -i "s/MYSQL_PORT/$MYSQL_PORT/g" {} +
find . -name "*.xml"  -exec sed -i "s/MYSQL_USER/$MYSQL_USER/g" {} +
find . -name "*.xml"  -exec sed -i "s/MYSQL_PWD/$MYSQL_PWD/g" {} +
find . -name "*.xml"  -exec sed -i "s/REDIS_HOST/$REDIS_HOST/g" {} +
find . -name "*.xml"  -exec sed -i "s/REDIS_PORT/$REDIS_PORT/g" {} +
find . -name "*.xml"  -exec sed -i "s/REDIS_PWD/$REDIS_PWD/g" {} +
find . -name "*.xml"  -exec sed -i "s/REDIS_DB_NUM/$REDIS_DB_NUM/g" {} +
find . -name "*.xml"  -exec sed -i "s/OUTER_IP_ADDR/$OUTER_IP_ADDR/g" {} +
find . -name "*.xml"  -exec sed -i "s/NODE_HOST/$NODE_HOST/g" {} +
find . -name "*.xml"  -exec sed -i "s/GATE_HOST/$GATE_HOST/g" {} +
find . -name "*.xml"  -exec sed -i "s/DB_HOST/$DB_HOST/g" {} +
find . -name "*.xml"  -exec sed -i "s/DISPATCH_HOST/$DISPATCH_HOST/g" {} +
find . -name "*.xml"  -exec sed -i "s/GAME_HOST/$GAME_HOST/g" {} +
find . -name "*.xml"  -exec sed -i "s/MUIP_HOST/$MUIP_HOST/g" {} +
find . -name "*.xml"  -exec sed -i "s/OA_HOST/$OA_HOST/g" {} +
find . -name "*.xml"  -exec sed -i "s/PATHFINDING_HOST/$PATHFINDING_HOST/g" {} +
find . -name "*.xml"  -exec sed -i "s/MULTI_HOST/$MULTI_HOST/g" {} +
find . -name "*.xml"  -exec sed -i "s/TOTHEMOON_HOST/$TOTHEMOON_HOST/g" {} +
cp /srv/bak/${SERVER_NAME}.xml /srv/${SERVER_NAME}/conf/

echo "============${SERVER_NAME} start=========="
echo "============ APP_ID: ${APP_ID}============"
echo "==========================================="

cd /srv/${SERVER_NAME}
if ! test -x ${SERVER_NAME}; then
  chmod a+x ${SERVER_NAME}
fi
./${SERVER_NAME} -i ${APP_ID} >/dev/null 2>&1 &
sleep 5
tail -f /srv/${SERVER_NAME}/log/*log*